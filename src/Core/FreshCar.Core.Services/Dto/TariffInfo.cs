﻿using FreshCar.Core.Entities;

namespace FreshCar.Core.Services
{
    /// <summary>
    /// Информация о тарифе и услуге
    /// </summary>
    public class TariffInfo
    {
        public int TariffId { get; set; }

        public CarType CarType { get; set; }

        public int WorkId { get; set; }

        public string WorkName { get; set; }
    }
}
