﻿using System.Linq;
using FreshCar.Core.Entities;

namespace FreshCar.Core.Services
{
    /// <summary>
    /// Репозиторий для работы с сущностями
    /// </summary>
    /// <typeparam name="TEntity">Тип сущности, с которой работает репозиторий</typeparam>
    public interface IRepository<TEntity> where TEntity : class, IEntity
    {
        /// <summary>
        /// Возвращает IQueryable коллекцию сущностей
        /// </summary>
        IQueryable<TEntity> Query();

        /// <summary>
        /// Добавляет новую сущность в коллекцию (без сохранения)
        /// </summary>
        TEntity Add(TEntity entity);

        /// <summary>
        /// Удаляет сущность из коллекции (без сохранения)
        /// </summary>
        void Remove(TEntity entity);

        /// <summary>
        /// Обновляет сущность в коллекции (без сохранения)
        /// </summary>
        TEntity Update(TEntity entity);
    }
}
