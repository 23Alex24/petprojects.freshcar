﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FreshCar.Core.Entities
{
    /// <summary>
    /// Дополнительные услуги автомойки (такие как кофе, бесплатный - wi-fi и т.д.)
    /// </summary>
    [Table("CarWashAdditionalWorks")]
    public class CarWashAdditionalWork : IEntity<int>
    {
        /// <summary>
        /// Id
        /// </summary>
        [Key]
        [Column("Id")]
        public int Id { get; set; }

        /// <summary>
        /// Id мойки
        /// </summary>
        [ForeignKey(nameof(CarWash))]
        [Column("CarWashId")]
        public int CarWashId { get; set; }

        /// <summary>
        /// Id доп. услуги
        /// </summary>
        [ForeignKey(nameof(Work))]
        [Column("WorkId")]
        public int WorkId { get; set; }



        /// <summary>
        /// Навигационное свойство. Автомойка.
        /// </summary>
        public virtual CarWash CarWash { get; set; }

        /// <summary>
        /// Навигационное свойство. Сервис
        /// </summary>
        public virtual Work Work { get; set; }
    }
}
