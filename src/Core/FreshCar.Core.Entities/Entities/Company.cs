﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FreshCar.Core.Entities
{
    /// <summary>
    /// Компания (фирма / сеть автомоек)
    /// </summary>
    [Table("Companies")]
    public class Company : IEntity<int>
    {
        public const int NAME_MAX_LENGTH = 50;
        public const int DESCRIPTION_MAX_LENGTH = 500;

        /// <summary>
        /// Id компании
        /// </summary>
        [Key]
        [Column("Id")]
        public int Id { get; set; }

        /// <summary>
        /// Название компании
        /// </summary>
        [Column("Name")]
        [MaxLength(NAME_MAX_LENGTH)]
        public string Name { get; set; }

        /// <summary>
        /// Описание компании
        /// </summary>
        [Column("Description")]
        [MaxLength(DESCRIPTION_MAX_LENGTH)]
        public string Description { get; set; }

        /// <summary>
        /// Баланс фирмы (сколько у них денег на счету в нашей системе)
        /// </summary>
        [Column("Balance")]
        public decimal Balance { get; set; }



        /// <summary>
        ///  Навигационное свойство. Автомойки этой компании (фирмы).
        /// </summary>
        public virtual ICollection<CarWash> CarWashes { get; set; }

        /// <summary>
        /// Навигационное свойство. Сотрудники фирмы
        /// </summary>
        public virtual ICollection<Employee> Employees { get; set; }

        /// <summary>
        /// Навигационное свойство. Услуги, предоставляемые фирмой (опционально)
        /// </summary>
        public virtual ICollection<Work> Works { get; set; }
    }
}
