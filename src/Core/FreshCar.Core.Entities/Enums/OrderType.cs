﻿namespace FreshCar.Core.Entities
{
    /// <summary>
    /// Тип заказа
    /// </summary>
    public enum OrderType
    {
        /// <summary>
        /// Автоматический (заказ создали из мобильного приложения)
        /// </summary>
        Automatic = 0,

        /// <summary>
        /// Ручной (заказ создали в админке вручную)
        /// </summary>
        Manual = 1
    }
}
