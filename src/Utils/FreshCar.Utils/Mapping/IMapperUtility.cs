﻿using System.Collections.Generic;

namespace FreshCar.Utils.Mapping
{
    /// <summary>
    /// Маппер объектов
    /// </summary>
    public interface IMapperUtility
    {
        /// <summary>
        /// Создает новый объект на основании исходного, скопировав данные из полей
        /// </summary>
        /// <typeparam name="TSource">Тип объекта источника данных</typeparam>
        /// <typeparam name="TDestination">Тип объекта, в который будут скопированы данные</typeparam>
        /// <param name="source">экземпляр объекта источника данных</param>
        /// <exception cref="MappingException">
        /// Исключение возникает, если маппинг не удался по каким-то причинам
        /// </exception>
        /// <returns>Экземпляр нового объекта</returns>
        TDestination Map<TSource, TDestination>(TSource source);


        /// <summary>
        /// Выполняет маппинг IEnumerable
        /// </summary>
        /// <exception cref="MappingException">
        /// Исключение возникает, если маппинг не удался по каким-то причинам
        /// </exception>
        IEnumerable<TDestination> MapEnumerable<TSource, TDestination>(IEnumerable<TSource> source);
    }
}
