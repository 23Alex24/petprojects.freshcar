﻿using System.Threading.Tasks;

namespace FreshCar.Utils.Geolocation
{
    /// <summary>
    /// Утилита геолокации
    /// </summary>
    public interface IGeolocationUtility
    {
        /// <summary>
        /// Получает местоположение по адресу
        /// </summary>
        /// <param name="city">Город</param>
        /// <param name="street">Улица</param>
        /// <param name="house">Дом</param>
        Task<GeolocationInfo> GetLocationByAddress(string city, string street, string house);
    }
}
