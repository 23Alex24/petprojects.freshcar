﻿namespace FreshCar.Utils.Geolocation
{
    /// <summary>
    /// Информация о местоположении
    /// </summary>
    public class GeolocationInfo
    {
        /// <summary>
        /// Широта
        /// </summary>
        public double Latitude { get; set; }

        /// <summary>
        /// Долгота
        /// </summary>
        public double Longitude { get; set; }
    }
}
