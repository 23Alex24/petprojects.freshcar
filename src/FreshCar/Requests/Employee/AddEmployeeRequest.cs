﻿using System.ComponentModel.DataAnnotations;
using FreshCar.Constants;
using Entities = FreshCar.Core.Entities;

namespace FreshCar.Requests.Employee
{
    /// <summary>
    /// Запрос на добавление нового сотрудника
    /// </summary>
    public class AddEmployeeRequest
    {
        /// <summary>
        /// Имя сотрудника
        /// </summary>
        [Required(ErrorMessage = "Заполните имя", AllowEmptyStrings = false)]
        [MaxLength(Entities.Employee.FIRST_NAME_MAX_LENGTH, ErrorMessage = "Максимальная допустимая длина имени: {1}")]
        public string FirstName { get; set; }

        /// <summary>
        /// Фамилия сотрудника
        /// </summary>
        [Required(ErrorMessage = "Заполните фамилию", AllowEmptyStrings = false)]
        [MaxLength(Entities.Employee.LAST_NAME_MAX_LENGTH, ErrorMessage = "Максимальная допустимая длина фамилии: {1}")]
        public string LastName { get; set; }

        /// <summary>
        /// Отчество сотрудника
        /// </summary>
        [Required(ErrorMessage = "Заполните отчество", AllowEmptyStrings = false)]
        [MaxLength(Entities.Employee.LAST_NAME_MAX_LENGTH, ErrorMessage = "Максимальная допустимая длина отчества: {1}")]
        public string MiddleName { get; set; }

        /// <summary>
        /// Телефон сотрудника
        /// </summary>        
        [Phone(ErrorMessage = "Неверный формат телефона")]
        [MaxLength(20, ErrorMessage = "Максимальная допустимая длина телефона: {1}")]
        [MinLength(5, ErrorMessage = "Минимальная допустимая длина телефона: {1}")]
        public string Phone { get; set; }

        /// <summary>
        /// Логин пользователя
        /// </summary>
        [Required(ErrorMessage = "Заполните логин", AllowEmptyStrings = false)]
        [EmailAddress(ErrorMessage = "Почтовый адрес должен быть в правильном формате")]
        [MinLength(5, ErrorMessage = "Минимальная допустимая длина логина: {1}")]
        [MaxLength(50, ErrorMessage = "Максимальная допустимая длина логина: {1}")]
        public string Login { get; set; }

        /// <summary>
        /// Пароль пользователя
        /// </summary>
        [Required(ErrorMessage = "Пароль не может быть пустым полем", AllowEmptyStrings = false)]
        [MinLength(6, ErrorMessage = "Длина пароля должна быть не менее {1}")]
        [MaxLength(20, ErrorMessage = "Длина пароля должна быть не более {1}")]
        [RegularExpression(RegexConstants.PASSWORD, ErrorMessage = "Пароль должен состоять из цифр или латинских букв")]
        public string Password { get; set; }
    }
}