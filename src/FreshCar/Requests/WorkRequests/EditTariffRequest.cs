﻿using System.ComponentModel.DataAnnotations;

namespace FreshCar.Requests.WorkRequests
{
    /// <summary>
    /// Запрос на изменение тарифа
    /// </summary>
    public class EditTariffRequest
    {
        public int Id { get; set; }

        [Range(typeof(decimal),"0","30000", ErrorMessage = "Стоимость должна быть от 0 до 30000 руб.")]
        public decimal Cost { get; set; }

        [Range(0, 300, ErrorMessage = "Неверное время")]
        public int Duration { get; set; }
    }
}