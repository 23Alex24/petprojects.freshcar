﻿using System.ComponentModel.DataAnnotations;
using FreshCar.Core.Entities;

namespace FreshCar.Requests.ApiMobile
{
    /// <summary>
    /// Запрос на получение списка автомоек для приложения
    /// </summary>
    public class GetCarWashesRequest
    {
        /// <summary>
        /// Цена, по которой надо фильтровать
        /// </summary>
        [Range(typeof(decimal), "0", "30000", ErrorMessage = "Стоимость должна быть от 0 до 30000 руб.")]
        public decimal? Price { get; set; }

        /// <summary>
        /// Id услуги (только системная)
        /// </summary>
        public int? WorkId { get; set; }

        [EnumDataType(typeof(CarType), ErrorMessage = "Некорректный тип машины")]
        public CarType? CarType { get; set; }

        /// <summary>
        /// Список айдишников доп. услуг, которые должны присутсвовать на автомойке
        /// </summary>
        public int[] AdditionalWorkIds { get; set; }
    }
}