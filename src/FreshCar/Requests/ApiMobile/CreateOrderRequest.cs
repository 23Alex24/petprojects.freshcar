﻿using System;
using System.ComponentModel.DataAnnotations;
using FreshCar.Attributes;
using FreshCar.Core.Entities;

namespace FreshCar.Requests.ApiMobile
{
    /// <summary>
    /// Запрос на создание заказа
    /// </summary>
    public class CreateOrderRequest
    {
        /// <summary>
        /// Id тарифа
        /// </summary>
        public int TariffId { get; set; }

        /// <summary>
        /// Дата записи, когда начнется мойка
        /// </summary>
        [NotFromPastDate(ErrorMessage = "Некорректная дата")]
        public DateTime StartDate { get; set; }

        /// <summary>
        /// Номер машины
        /// </summary>
        [Required(ErrorMessage = "Укажите номер машины", AllowEmptyStrings = false)]
        [MaxLength(Order.CAR_NUMBER_MAX_LENGTH, ErrorMessage = "Максимальная длина номера машины: {1}")]
        public string CarNumber { get; set; }

        /// <summary>
        /// Телефон клиента
        /// </summary>
        [Required(ErrorMessage = "Укажите номер телефона", AllowEmptyStrings = false)]
        [Phone(ErrorMessage = "Неверный формат телефона")]
        [MaxLength(Order.CLIENT_PHONE_MAX_LENGTH, ErrorMessage = "Максимальная длина телефона: {1}")]
        public string ClientPhone { get; set; }
    }
}