﻿using System;
using System.ComponentModel.DataAnnotations;
using FreshCar.Attributes;
using FreshCar.Core.Entities;

namespace FreshCar.Requests.CarWashRequests
{
    public class EditCarWashRequest
    {
        [Required(ErrorMessage = "Заполните название", AllowEmptyStrings = false)]
        [MaxLength(CarWash.NAME_MAX_LENGTH, ErrorMessage = "Максимальная длина имени: {1}")]
        public string Name { get; set; }

        /// <summary>
        /// Время начала рабочего дня в UTC
        /// </summary>
        [DateLaterThan(10,03,2016, ErrorMessage = "Невалидная дата")]
        public DateTime StartTimeOfWork { get; set; }

        /// <summary>
        /// Время окончания рабочего дня в UTC
        /// </summary>
        [DateLaterThan(10, 03, 2016, ErrorMessage = "Невалидная дата")]
        public DateTime EndTimeOfWork { get; set; }

        [Required(ErrorMessage = "Заполните телефон", AllowEmptyStrings = false)]
        [Phone(ErrorMessage = "Неверный формат телефона")]
        [MaxLength(CarWash.PHONE_MAX_LENGTH, ErrorMessage = "Максимальная длина телефона: {1}")]
        public string Phone { get; set; }
    }
}