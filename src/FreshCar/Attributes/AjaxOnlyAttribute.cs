﻿using System.Web.Mvc;

namespace FreshCar.Attributes
{
    public class AjaxOnly : ActionMethodSelectorAttribute
    {
        public override bool IsValidForRequest(ControllerContext controllerContext, System.Reflection.MethodInfo methodInfo)
        {
            var result =  controllerContext.HttpContext.Request.IsAjaxRequest();
            return result;
        }
    }
}