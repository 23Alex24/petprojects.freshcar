﻿using System;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FreshCar.Constants;
using Microsoft.Ajax.Utilities;

namespace FreshCar.Attributes
{
    /// <summary>
    /// Запрещает заходить авторизованному пользователю
    /// </summary>
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class)]
    public class DenyAuthorizedAttribute : ActionFilterAttribute
    {
        private string RedirectUrl { get; }

        /// <summary>Конструктор</summary>
        /// <param name="redirectUrl">Урл, на который надо сделать редирект, 
        /// если пользователь авторизован</param>
        public DenyAuthorizedAttribute(string redirectUrl = AttributeConstants.Urls.MAIN_PAGE)
        {
            RedirectUrl = redirectUrl.IsNullOrWhiteSpace() ? AttributeConstants.Urls.MAIN_PAGE : redirectUrl;
        }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            var isAjaxRequest = context.RequestContext.HttpContext.Request.IsAjaxRequest();
            var user = HttpContext.Current.User;

            if (user?.Identity == null || !user.Identity.IsAuthenticated)
                return;

            if (isAjaxRequest)
            {
                context.Result = new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            else context.Result = new RedirectResult(RedirectUrl);
        }
    }
}