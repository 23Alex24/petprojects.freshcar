﻿using Microsoft.AspNet.Identity;

namespace FreshCar.Autentication
{
    /// <summary>
    /// Этот класс необходим для работы asp.net identity
    /// </summary>
    public class UserManager : UserManager<ApplicationUser>
    {
        private readonly IUserStore<ApplicationUser> _store;

        public UserManager(IUserStore<ApplicationUser> store) : base(store)
        {
            _store = store;
        }
    }
}