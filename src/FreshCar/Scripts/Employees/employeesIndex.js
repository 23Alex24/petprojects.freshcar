﻿var rootUrl = window.location.protocol + "//" + window.location.host + "/",
    mobile = false;

var dataSource = new kendo.data.DataSource({
    /* data: [
        { Id: 1, Surname: "Иванов", Name: "Иван", Patronymic: "Иванович", Login: "Vanya", Post: "Рабочий", PhoneNumber: "9876543210" },
        { Id: 2, Surname: "Сидоров", Name: "Сидр", Patronymic: "Сидорович", Login: "Sidr", Post: "Рабочий", PhoneNumber: "9876543211" },
        { Id: 3, Surname: "Петров", Name: "Пётр", Patronymic: "Петрович", Login: "Petr", Post: "Менеджер", PhoneNumber: "9876543212" }
    ],*/
    transport: {
        read: {
            url: rootUrl + "Employee/GetEmployees",
            dataType: "json"
        }
    },
    schema: {
        model: {
            id: "Id",
            fields: {
                Id: { editable: false, nullable: true },
                LastName: { validation: { required: true } },
                FirstName: { validation: { required: true } },
                MiddleName: { validation: { required: true } },
                Role: { validation: { required: true } },
                Login: { validation: { required: true } },
                Phone: { validation: { required: true } }
            }
        }/*,
        parse: function(data) {
            return data.CarWashEmployeeInfo;
        }*/
    }
});

//--------------------------------
//  Инициализация страницы сотрудников
//--------------------------------
$(document).ready(function() {

    $("li.active").removeClass("active");
    $("a[data-id=5]").parent("li").addClass("active");
    //Опции для окна
    $("#add-employee").kendoWindow({
        title: "",
        draggable: false,
        modal: true,
        pinned: true,
        resizable: false
    });
    //Инициализация грида
    $("#grid").kendoGrid({
        dataSource: dataSource,
        // mobile: true,
        height: $(document).height() - 90,
        toolbar: kendo.template($("#toolBarTemplate").html()),
        columns: [
            { field: "LastName", title: "Фамилия", width: "140px" },
            { field: "FirstName", title: "Имя", width: "120px" },
            { field: "MiddleName", title: "Отчество", width: "120px" },
            { field: "Role", title: "Должность", values: [{ text: "Администратор", value: 1 }], width: "150px" },
            { field: "Login", title: "Логин", width: "150px" },
            { field: "Phone", title: "Телефон", width: "160px" },
            { template: kendo.template($("#employeeCommandsCellTemplate").html()), width: "240px", title: " " }
        ]
    });
});

//--------------------------------
//  Уведомления
//--------------------------------
//Ошибка
function showError(msg) {
    $("#error-msg").text(msg);
    $("#error-block").show();
}
//Успех
function showSuccess(msg) {
    $("#success-msg").text(msg);
    $("#success-block").show();
}
//Скрытие обоих блоков
function hideMessages() {
    $("#success-block").hide();
    $("#error-block").hide();
}

//--------------------------------
//  Создание сотрудника    
//--------------------------------
//Сохранение нового сотрудника
function addEmployeeSubmit(e) {
    hideMessages();
    //Валидация
    if (!validate("#add-employee", "add")) {
        e.preventDefault();
        return false;
    }
    //Делаем запрос на сервер
    $.ajax({
        url: "Employee/AddEmployee",
        type: "POST",
        data: {
            FirstName: $("#name").val(),
            LastName: $("#surname").val(),
            MiddleName: $("#patronymic").val(),
            Phone: $("#phone").val(),
            Login: $("#login").val(),
            Password: $("#password").val()
        },
        beforeSend: function () {
            $(".cssload-jumping").show();
        },
        success: function(response) {
            $(".cssload-jumping").hide();
            if (response.ErrorCode !== 0) {
                showError(response.ErrorMessage);
                alert(response.ErrorMessage);
            } else {
                $("#grid").data("kendoGrid").dataSource.read();
                showSuccess("Сотрудник успешно добавлен!");
                alert("Сотрудник успешно добавлен!");
                $("#add-employee").data("kendoWindow").close();
            }
        }
    });
}
//Событие всплывающего окна с добавлением сотрудника
function addEmployee() {
    //Очищаем форму перед добавлением
    $("#add-employee").find("input").val("").css("border","");
    //Назначаем обработчик на добавление сотрудника
    $("#save").unbind("click");
    $("#save").text("Сохранить");
    $("#save").bind("click", addEmployeeSubmit);
    //Скрываем подсказку для пароля
    $("#tooltip").hide();
    $("#password").val("");
    //Задаём маску для телефона
    $("#phone").kendoMaskedTextBox({
        mask: "+7 (000) 000-00-00"
        //promptChar: " "
    });
    //Выводим окно с попапом
    var win = $("#add-employee").data("kendoWindow");
    win.title("Новый сотрудник");
    if (mobile) {
        win.toFront().open();
    } else {
        win.center().open();
    }
}
//Событие отмены добавления или редактирования сотрудника
function cancelAddEmployee() {
    $("#add-employee").data("kendoWindow").close();
}
//--------------------------------
//  Редактирование сотрудника    
//--------------------------------
function editEmployeeSubmit(e) {
    hideMessages();
    //Валидация
    if (!validate("#add-employee")) {
        e.preventDefault();
        return false;
    }
    //Делаем запрос на сервер
    $.ajax({
        url: "Employee/EditEmployee",
        type: "POST",
        data: {
            EmployeeId: this.Id,
            FirstName: $("#name").val(),
            LastName: $("#surname").val(),
            MiddleName: $("#patronymic").val(),
            Phone: $("#phone").val(),
            Login: $("#login").val(),
            Password: $("#password").val()
        },
        beforeSend: function () {
            $(".cssload-jumping").show();
        },
        success: function (response) {
            $(".cssload-jumping").hide();
            if (response.ErrorCode !== 0) {
                showError(response.ErrorMessage);
                alert(response.ErrorMessage);
            } else {
                $("#grid").data("kendoGrid").dataSource.read();
                showSuccess("Информация о сотруднике успешно изменена!");
                alert("Информация о сотруднике успешно изменена!");
                $("#add-employee").data("kendoWindow").close();
            }
        }
    });
}
//Событие всплывающего окна с редактированием сотрудника
function editEmployee(e) {
    //Вытаскиваем всю информацию о сотруднике по айдишнику
    var employee = dataSource.get($(e).data("id"));
    //Вставляем в поля
    $("#surname").val(employee.LastName);
    $("#name").val(employee.FirstName);
    $("#patronymic").val(employee.MiddleName);
    $("#login").val(employee.Login);
    $("#password").val("");
    $("#phone").val(employee.Phone);
    //Назначаем обработчик на редатирование сотрудника
    $("#save").unbind("click");
    $("#save").text("Обновить");
    $("#save").bind("click", $.proxy(editEmployeeSubmit, employee));
    //Задаём маску для телефона
    $("#phone").kendoMaskedTextBox({
        mask: "+7 (000) 000-00-00"
        //promptChar: " "
    });
    //Скрываем подсказку для пароля
    $("#tooltip").show();
    $("#add-employee").find("input").css("border", "");
    //Выводим окно с попапом
    var win = $("#add-employee").data("kendoWindow");
    win.title("Изменение данных");
    if (mobile) {
        win.toFront().open();
    } else {
        win.center().open();
    }
}

//--------------------------------
//  Удаление сотрудника    
//--------------------------------
function removeEmployee(e) {
    hideMessages();
    //Вытаскиваем всю информацию о сотруднике по айдишнику
    var employee = dataSource.get($(e).data("id"));
    if (confirm("Вы действительно хотите удалить пользователя '" + employee.LastName + " " + employee.FirstName + "' из системы?")) {
        //Запрос на сервер
        $.ajax({
            url: "Employee/DeleteEmployee",
            type: "POST",
            data: {
                EmployeeId: employee.Id
            },
            beforeSend: function () {
                $(".cssload-jumping").show();
            },
            success: function (response) {
                $(".cssload-jumping").hide();
                if (response.ErrorCode !== 0) {
                    showError(response.ErrorMessage);
                    alert(response.ErrorMessage);
                } else {
                    $("#grid").data("kendoGrid").dataSource.read();
                    showSuccess("Сотрудник успешно удалён из системы!");
                    alert("Сотрудник успешно удалён из системы!");
                }
            }
        });
    }
}

//--------------------------------
//  Валидация полей   
//--------------------------------
function validate(elem,ev) {

    var inputs = $(elem).find("input");
    inputs.css("border", "");

    for (var i = 0; i < inputs.length; i++) {
        if (inputs[i].id !== "login" && inputs[i].id !== "password" && inputs[i].id !== "phone") {
            if (inputs[i].value.length < 2 || inputs[i].value.length > 150) {
                $(inputs[i]).css("border", "1px solid red");
                alert("Длина строки должна быть от 2 до 150 символов");
                return false;
            }
        } else if (inputs[i].id === "login") {
            if (!/^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i.test(inputs[i].value)) {
                $(inputs[i]).css("border", "1px solid red");
                alert("Логин должен быть электронной почтой");
                return false;
            }
        } else if (inputs[i].id === "password" && ev === "add") {
            if (inputs[i].value.length < 6 || inputs[i].value.length > 20) {
                $(inputs[i]).css("border", "1px solid red");
                alert("Пароль должен быть не менее 6 и не более 20 символов");
                return false;
            }
        }
    }
    return true;
}