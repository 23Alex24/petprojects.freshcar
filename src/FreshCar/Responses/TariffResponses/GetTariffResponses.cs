﻿using FreshCar.Models.TariffModels;

namespace FreshCar.Responses.TariffResponses
{
    public class GetTariffResponses : BaseResponse
    {
        public TariffModel Tariff { get; set; }
    }
}