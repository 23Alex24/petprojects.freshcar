﻿namespace FreshCar.Responses.ApiMobile
{
    /// <summary>
    /// Ответ для метода GetWorkPrice
    /// </summary>
    public class GetTariffResponse : BaseResponse
    {
        /// <summary>
        /// Айди тарифа
        /// </summary>
        public int TariffId { get; set; }

        /// <summary>
        /// Стоимость услуги
        /// </summary>
        public decimal Cost { get; set; }

        /// <summary>
        /// Время в минутах (сколько эту услугу делают по времени)
        /// </summary>
        public int Duration { get; set; }
    }
}