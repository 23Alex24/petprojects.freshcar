﻿using FreshCar.Models.WorkModels;

namespace FreshCar.Responses.ApiMobile
{
    /// <summary>
    /// Ответ для метода получения всех системных услуг
    /// </summary>
    public class GetSystemWorksResponse : BaseResponse
    {
        /// <summary>
        /// Услуги
        /// </summary>
        public WorkShortInfoModel[] Works { get; set; }
    }
}