﻿using FreshCar.Models.CarWashModels;

namespace FreshCar.Responses.ApiMobile
{
    /// <summary>
    /// Респонс на метод GetCarWashes
    /// </summary>
    public class GetCarWashesResponse : BaseResponse
    {
        /// <summary>
        /// Автомойки
        /// </summary>
        public CarWashShortInfoModel[] CarWashes { get; set; }
    }
}