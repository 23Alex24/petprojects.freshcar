﻿using FreshCar.Models.CarWashModels;
using FreshCar.Models.WorkModels;

namespace FreshCar.Responses.ApiMobile
{
    /// <summary>
    /// Ответ для метода получения информации об автомойке
    /// </summary>
    public class GetCarWashResponse : BaseResponse
    {
        /// <summary>
        /// Информация об автомойке
        /// </summary>
        public CarWashModel CarWashInfo { get; set; }

        /// <summary>
        /// Доп. услуги, которые присутствуют на этой автомойке (кофе...)
        /// </summary>
        public WorkShortInfoModel[] AdditionalWorks { get; set; }

        /// <summary>
        /// Ссылки на фотографии
        /// </summary>
        public string[] Photos { get; set; }
    }
}