﻿namespace FreshCar.Models.CarWashModels
{
    /// <summary>
    /// Модель для отображения информации об автомойке
    /// </summary>
    public class CarWashModel
    {
        /// <summary>
        /// Id автомойки
        /// </summary>
        public int Id { get; set; }


        /// <summary>
        /// Название автомойки
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Время начала рабочего дня (сюда записывать только UTC время)
        /// </summary>
        public TimeModel StartTimeOfWork { get; set; }

        /// <summary>
        /// Время окончания рабочего дня (сюда записывать только UTC время)
        /// </summary>
        public TimeModel EndTimeOfWork { get; set; }

        /// <summary>
        /// Номер телефона
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// Город, в котором находится автомойка
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Улица, на которой находится автомойка
        /// </summary>
        public string Street { get; set; }

        /// <summary>
        /// Номер дома автомойки
        /// </summary>
        public string HouseNumber { get; set; }

        /// <summary>
        /// Широта (местоположение автомойки)
        /// </summary>
        public double Latitude { get; set; }

        /// <summary>
        /// Долгота (местоположение автомойки)
        /// </summary>
        public double Longitude { get; set; }
    }
}