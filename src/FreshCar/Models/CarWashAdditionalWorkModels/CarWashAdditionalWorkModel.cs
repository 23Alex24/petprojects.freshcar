﻿namespace FreshCar.Models.CarWashAdditionalWorkModels
{
    public class CarWashAdditionalWorkModel
    {
        public string Name { get; set; }

        public int WorkId { get; set; }

        public bool IsActive { get; set; }
    }
}