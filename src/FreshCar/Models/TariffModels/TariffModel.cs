﻿using FreshCar.Core.Entities;

namespace FreshCar.Models.TariffModels
{
    public class TariffModel
    {
        public int Id { get; set; }

        public decimal Cost { get; set; }

        public int Duration { get; set; }

        public CarType CarType { get; set; }
    }
}
