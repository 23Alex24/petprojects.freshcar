﻿namespace FreshCar.Models
{
    /// <summary>
    /// Модель для передачи времени
    /// </summary>
    public class TimeModel
    {
        /// <summary>
        /// Часы
        /// </summary>
        public int Hours { get; set; }

        /// <summary>
        /// Минуты
        /// </summary>
        public int Minutes { get; set; }
    }
}