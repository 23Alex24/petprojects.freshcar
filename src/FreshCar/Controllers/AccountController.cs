﻿using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using FreshCar.Attributes;
using FreshCar.Autentication;
using FreshCar.Business.Commands;
using FreshCar.Constants;
using FreshCar.Extensions;
using FreshCar.Requests.AccountRequests;
using FreshCar.Responses;
using Microsoft.AspNet.Identity;

namespace FreshCar.Controllers
{
    /// <summary>
    /// Контроллер для управления аккаунтом
    /// </summary>
    public class AccountController : BaseController
    {
        private readonly UserManager<ApplicationUser> _userManager =
            new UserManager<ApplicationUser>(new UserStore());

        [DenyAuthorized, HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Метод для входа в лк
        /// </summary>
        [DenyAuthorized, HttpPost, ValidateRequest, AjaxOnly, ValidateAntiForgeryToken]
        public async Task<JsonResult> SignIn(SignInRequest request)
        {
            var command = DependencyResolver.Current
                .GetService<ICommand<AuthorizeUserCommandArgs, AuthorizeUserCommandResult>>();

            var arguments = CreateCommandArgs<AuthorizeUserCommandArgs>();
            arguments.Login = request.Login?.Trim();
            arguments.Password = request.Password?.Trim();
            var result = await command.Execute(arguments);

            if (!result.IsSuccess)
            {
                return Json(result.ConverToResponse<EmptyResponse>());
            }

            SaveAccountInCookies(result);
            return Json(result.ConverToResponse<EmptyResponse>());
        }

        /// <summary>
        /// Метод для выхода из лк
        /// </summary>
        /// <returns></returns>
        [Authorize, HttpPost]
        public ActionResult SignOut()
        {
            var manager = HttpContext.GetOwinContext().Authentication;
            manager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Index", "Account");
        }

        /// <summary>
        /// Метод для регистрации нового пользователя админки
        /// </summary>
        [DenyAuthorized, HttpPost, ValidateRequest, AjaxOnly]
        public async Task<JsonResult> Register(RegisterRequest request)
        {
            var command = DependencyResolver.Current
                .GetService<ICommand<RegisterUserCommandArgs, EmptyCommandResult>>();

            var arguments = CreateCommandArgs<RegisterUserCommandArgs>();
            arguments.Email = request.Email?.Trim();
            arguments.Phone = request.Phone?.Trim();
            arguments.CompanyName = request.CompanyName?.Trim();
            arguments.House = request.House?.Trim();
            arguments.Street = request.Street?.Trim();
            arguments.City = request.City?.Trim();
            arguments.Password = request.Password?.Trim();
            var result = await command.Execute(arguments);
        
            return Json(result.ConverToResponse<EmptyResponse>());
        }
        
        /// <summary>
        /// Сохраняет в куки инфу о юзере
        /// </summary>
        private void SaveAccountInCookies(AuthorizeUserCommandResult account)
        {
            var identity = _userManager.CreateIdentity(new ApplicationUser
            {
                Id = account.UserInfo.AccountId.ToString(),
                UserName = "adfsdf"
            }, DefaultAuthenticationTypes.ApplicationCookie);

            identity.AddClaim(new Claim(ClaimTypes.Email, account.UserInfo.Email.ToLower()));

            if (account.UserInfo.Role.HasValue)
            {
                identity.AddClaim(new Claim(ClaimTypes.Role, account.UserInfo.Role.ToString().ToLower()));
            }

            if (account.UserInfo.CarWashId.HasValue)
            {
                identity.AddClaim(new Claim(CustomClaimsConstants.CAR_WASH_ID, account.UserInfo.CarWashId.ToString()));
            }

            if (account.UserInfo.CompanyId.HasValue)
            {
                identity.AddClaim(new Claim(CustomClaimsConstants.COMPANY_ID, account.UserInfo.CompanyId.ToString()));
            }

            if (account.UserInfo.EmployeeId.HasValue)
            {
                identity.AddClaim(new Claim(CustomClaimsConstants.EMPLOYEE_ID, account.UserInfo.EmployeeId.ToString()));
            }

            var manager = HttpContext.GetOwinContext().Authentication;
            manager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            manager.SignIn(identity);
        }
    }
}
