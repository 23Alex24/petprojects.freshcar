﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FreshCar.Business.Errors;
using FreshCar.Core.Services;
using FreshCar.Extensions;
using FreshCar.Models.CarWashAdditionalWorkModels;
using FreshCar.Responses.CarWashAdditionalWork;

namespace FreshCar.ResponseBuilders.CarWashAdditionalWork
{
    public class GetCarWashAdditionalWorkResponseBuilder
    {
        private readonly IWorkService _workService;

        public GetCarWashAdditionalWorkResponseBuilder(IWorkService workService)
        {
            _workService = workService;
        }

        public async Task<GetAdditionalWorksResponseBuilder> Build(UserInfo currentUser)
        {
            var response = new GetAdditionalWorksResponseBuilder();
            var works = new List<CarWashAdditionalWorkModel>();

            try
            {
                var systemWorks = await _workService.GetAdditionalSystemWorksAsync();
                var carWashWorks = await _workService.GetAdditionalWorksAsync(currentUser.CarWashId.Value, true);

                foreach (var systemWork in systemWorks)
                {
                    var isActive = carWashWorks.Any(x => x.Id == systemWork.Id);
                    works.Add(new CarWashAdditionalWorkModel()
                    {
                        Name = systemWork.Name,
                        WorkId = systemWork.Id,
                        IsActive = isActive
                    });
                }
                response.CarWashAdditionalWorks = works.ToArray();
            }
            catch (Exception)
            {
                return response.AddError(ErrorConstants.System.INTERNAL_SERVER_ERROR, 500);
            }

            return response;
        }
    }
}