﻿using System.Collections.Generic;
using System.Web.Mvc;
using FreshCar.Core.Entities;
using FreshCar.Core.Services;
using FreshCar.Models.Home;

namespace FreshCar.ResponseBuilders.Home
{
    public class LeftMenuModelBuilder
    {
        public List<LeftMenuItemModel> Build(UserInfo currentUser)
        {
            var result = new List<LeftMenuItemModel>();

            result.Add(new LeftMenuItemModel()
            {
                Name = "Главная",
                Controller = "Home",
                Action = "Index",
                CssClass = "active",
                IconClass = "fa fa-calendar",
                DataId = 1
            });

            result.Add(new LeftMenuItemModel()
            {
                Name = "Услуги",
                Controller = "Work",
                Action = "Index",
                IconClass = "fa fa-tasks",
                DataId = 2
            });

            if (currentUser.Role == EmployeeRole.Director)
            {
                result.Add(new LeftMenuItemModel()
                {
                    Name = "Статистика",
                    Controller = "Statistic",
                    Action = "Index",
                    IconClass = "fa fa-pie-chart",
                    DataId = 3
                });
            }

            result.Add(new LeftMenuItemModel()
            {
                Name = "Боксы",
                Controller = "Box",
                Action = "Index",
                IconClass = "fa fa-building-o",
                DataId = 4
            });

            if (currentUser.Role == EmployeeRole.Director)
            {
                result.Add(new LeftMenuItemModel()
                {
                    Name = "Сотрудники",
                    Controller = "Employee",
                    Action = "Index",
                    IconClass = "fa fa-users",
                    DataId = 5
                });
            }

            result.Add(new LeftMenuItemModel()
            {
                Name = "Настройки",
                Controller = "CarWash",
                Action = "Index",
                IconClass = "fa fa-cogs",
                DataId = 6
            });

            return result;
        }
    }
}