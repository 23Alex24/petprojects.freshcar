﻿using System;
using System.Linq;
using System.Threading.Tasks;
using FreshCar.Business.Errors;
using FreshCar.Core.Services;
using FreshCar.Extensions;
using FreshCar.Models.TariffModels;
using FreshCar.Responses.WorkReponses;

namespace FreshCar.ResponseBuilders
{
    public class GetWorkTariffsResponseBuilder
    {
        private readonly IWorkService _workService;

        public GetWorkTariffsResponseBuilder(IWorkService workService)
        {
            _workService = workService;
        }

        public async Task<GetWorkTariffsResponse> Build(int id, UserInfo currentUser)
        {
            var response = new GetWorkTariffsResponse();

            try
            {
                var tariffs = await _workService.GetTariffsAsync(currentUser.CarWashId.Value, id, true);
                response.Tariffs = tariffs.Select(x => new TariffModel()
                {
                    Id = x.Id,
                    CarType = x.CarType,
                    Cost = x.Cost,
                    Duration = x.Duration
                }).ToArray();
            }
            catch (Exception)
            {
                return response.AddError(ErrorConstants.System.INTERNAL_SERVER_ERROR, 500);
            }

            return response;
        }
    }
}