﻿using System;
using System.Linq;
using System.Threading.Tasks;
using FreshCar.Business.Errors;
using FreshCar.Core.Entities;
using FreshCar.Core.Services;
using FreshCar.Extensions;
using FreshCar.Models.WorkModels;
using FreshCar.Responses.WorkReponses;
using FreshCar.Utils.Mapping;

namespace FreshCar.ResponseBuilders
{
    public class GetMainWorksResponseBuilder
    {
        private readonly IWorkService _workService;
        private readonly IMapperUtility _mapper;

        public GetMainWorksResponseBuilder(IWorkService workService, IMapperUtility mapper)
        {
            _workService = workService;
            _mapper = mapper;
        }

        public async Task<GetAllWorksResponse> Build(int companyId)
        {
            var response = new GetAllWorksResponse();

            try
            {
                var works = await _workService.GetMainWorksAsync(companyId);
                response.Works = works.Select(x => new WorkShortInfoModel()
                {
                    Name = x.Name,
                    Id = x.Id,
                    IsSystem = x.WorkType == WorkType.MainSystem
                }).ToArray();
            }
            catch (Exception)
            {
                return response.AddError(ErrorConstants.System.INTERNAL_SERVER_ERROR, 500);
            }

            return response;
        }
    }
}