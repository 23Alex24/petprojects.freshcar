﻿using System;
using System.Threading.Tasks;
using FreshCar.Business.Errors;
using FreshCar.Core.Services;
using FreshCar.Extensions;
using FreshCar.Requests.ApiMobile;
using FreshCar.Responses.ApiMobile;

namespace FreshCar.ResponseBuilders.ApiMobile
{
    /// <summary>
    /// Билдер для GetTariffResponse
    /// </summary>
    public class GetTariffResponseBuilder
    {
        private readonly IWorkService _workService;

        public GetTariffResponseBuilder(IWorkService workService)
        {
            _workService = workService;
        }

        public async Task<GetTariffResponse> Build(GetTariffRequest request)
        {
            var result = new GetTariffResponse();

            try
            {
                var tariff = await _workService.GetTariffAsync(request.CarWashId, request.WorkId, request.CarType);

                if (tariff == null)
                    return result.AddError(ErrorConstants.Tariff.TARIFF_NOT_FOUND, 404);

                result.TariffId = tariff.Id;
                result.Cost = tariff.Cost;
                result.Duration = tariff.Duration;
                return result;
            }
            catch (Exception)
            {
                return result.AddError(ErrorConstants.System.INTERNAL_SERVER_ERROR, 500);
            }
        }
    }
}