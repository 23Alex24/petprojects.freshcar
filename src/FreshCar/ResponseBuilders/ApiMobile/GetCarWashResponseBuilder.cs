﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Entity = FreshCar.Core.Entities;
using FreshCar.Business.Errors;
using FreshCar.Core.Services;
using FreshCar.Extensions;
using FreshCar.Models.CarWashModels;
using FreshCar.Models.WorkModels;
using FreshCar.Requests.ApiMobile;
using FreshCar.Responses.ApiMobile;
using FreshCar.Utils.Mapping;

namespace FreshCar.ResponseBuilders.ApiMobile
{
    /// <summary>
    /// Билдер для ответа GetCarWashResponse
    /// </summary>
    public class GetCarWashResponseBuilder
    {
        private readonly IWorkService _workService;
        private readonly ICarWashService _carWashService;
        private readonly IMapperUtility _mapperUtility;

        public GetCarWashResponseBuilder(IWorkService workService, 
            ICarWashService carWashService,
            IMapperUtility mapperUtility)
        {
            _workService = workService;
            _carWashService = carWashService;
            _mapperUtility = mapperUtility;
        }

        public async Task<GetCarWashResponse> Build(GetCarWashRequest request)
        {
            var result = new GetCarWashResponse();

            try
            {
                var carWash = await _carWashService.GetCarWashAsync(request.CarWashId);

                if (carWash == null)
                    return result.AddError(ErrorConstants.CarWash.CAR_WASH_NOT_FOUND, 400);

                result.CarWashInfo = _mapperUtility.Map<Entity.CarWash, CarWashModel>(carWash);

                var additionalWorks = await _workService.GetAdditionalWorksAsync(request.CarWashId, true);
                result.AdditionalWorks = _mapperUtility
                    .MapEnumerable<Entity.Work, WorkShortInfoModel>(additionalWorks).ToArray();

                var photos = await _carWashService.GetPhotos(request.CarWashId);
                if (photos != null)
                {
                    result.Photos = photos.Select(x => x.PhotoUrl).ToArray();
                }
            }
            catch (Exception ex)
            {
                result.AddError(ErrorConstants.System.INTERNAL_SERVER_ERROR, 500);
            }

            return result;
        }
    }
}