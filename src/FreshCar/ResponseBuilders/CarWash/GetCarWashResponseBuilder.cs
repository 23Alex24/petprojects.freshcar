﻿using System;
using Entity = FreshCar.Core.Entities;
using System.Threading.Tasks;
using FreshCar.Core.Services;
using FreshCar.Responses.CarWash;
using FreshCar.Utils.Mapping;
using FreshCar.Business.Errors;
using FreshCar.Extensions;
using FreshCar.Models.CarWashModels;

namespace FreshCar.ResponseBuilders.CarWash
{
    public class GetCarWashResponseBuilder
    {
        private readonly ICarWashService _carWashService;
        private readonly IMapperUtility _mapper;

        public GetCarWashResponseBuilder(ICarWashService carWashService, IMapperUtility mapper)
        {
            _carWashService = carWashService;
            _mapper = mapper;
        }

        public async Task<GetCarWashResponse> Build(int carWashId)
        {
            var response = new GetCarWashResponse();

            try
            {
                var carWash = await _carWashService.GetCarWashAsync(carWashId);

                if (carWash != null)
                {
                    response.CarWash = _mapper.Map<Entity.CarWash, CarWashModel>(carWash);
                }
            }
            catch(Exception)
            {
                return response.AddError(ErrorConstants.System.INTERNAL_SERVER_ERROR, 500);
            }

            return response;
        }
    }
}