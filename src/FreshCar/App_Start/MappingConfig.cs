﻿using System;
using AutoMapper;
using FreshCar.Business.Commands.Order;
using FreshCar.Core.Entities;
using FreshCar.Core.Services;
using FreshCar.Infrastructure.Utils.Mapping.Extensions;
using FreshCar.Models;
using FreshCar.Models.CarWashModels;
using FreshCar.Models.OrderModels;
using FreshCar.Models.WorkModels;
using FreshCar.Requests.OrderRequests;

namespace FreshCar
{
    public class MappingConfig
    {
        public static MapperConfiguration Configure()
        {
            var configuration = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<CarWash, CarWashShortInfoDto>()
                    .IgnoreSourceMember(x => x.Company)
                    .IgnoreSourceMember(x => x.CarWashAdditionalWorks)
                    .IgnoreSourceMember(x => x.CarWashEmployees)
                    .IgnoreSourceMember(x => x.CarWashBoxes)
                    .IgnoreSourceMember(x => x.Tariffs);

                cfg.CreateMap<CarWashShortInfoDto, CarWashShortInfoModel>();

                cfg.CreateMap<TimeSpan, TimeModel>()
                    .ConvertUsing(x => new TimeModel()
                    {
                        Hours = x.Hours,
                        Minutes = x.Minutes
                    });

                cfg.CreateMap<CarWash, CarWashModel>()
                    .IgnoreSourceMember(x => x.Company)
                    .IgnoreSourceMember(x => x.CarWashAdditionalWorks)
                    .IgnoreSourceMember(x => x.CarWashEmployees)
                    .IgnoreSourceMember(x => x.CarWashBoxes)
                    .IgnoreSourceMember(x => x.Tariffs);

                cfg.CreateMap<Work, WorkShortInfoModel>()
                    .ConvertUsing(x => new WorkShortInfoModel()
                    {
                        Name = x.Name,
                        Id = x.Id
                    });

                cfg.CreateMap<Order, OrderModel>()
                    .IgnoreSourceMember(x => x.CarWashBox)
                    .IgnoreSourceMember(x => x.Tariff);

                cfg.CreateMap<CreateOrderRequest, CreateOrderFromAdminCommandArgs>();
                cfg.CreateMap<EditOrderRequest, EditOrderFromAdminArgs>();
            });

            return configuration;
        }
    }
}