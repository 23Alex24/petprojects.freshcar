﻿using System;
using System.Linq.Expressions;
using AutoMapper;

namespace FreshCar.Infrastructure.Utils.Mapping.Extensions
{
    /// <summary>
    /// Расширения для упрощения настройки маппера
    /// </summary>
    public static class MapperConfigExtensions
    {
        /// <summary>
        /// Игнорирует Свойство источника
        /// </summary>
        /// <param name="sourceMember">Свойство, которое нужно игнорировать</param>
        public static IMappingExpression<TSource, TDestination> IgnoreSourceMember<TSource, TDestination>(
            this IMappingExpression<TSource, TDestination> mappingExpression,
            Expression<Func<TSource, object>> sourceMember)
        {
            return mappingExpression.ForSourceMember(sourceMember, opt => opt.Ignore());
        }
    }
}
