namespace FreshCar.Infrastructure.Core.Services.EF
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Accounts",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Login = c.String(nullable: false, maxLength: 50),
                        PasswordHash = c.String(nullable: false, maxLength: 500),
                        AccountState = c.Int(nullable: false),
                        EmployeeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Employees", t => t.EmployeeId, cascadeDelete: true)
                .Index(t => t.Login, unique: true, name: "UniqueEmail")
                .Index(t => t.EmployeeId, unique: true, name: "UniqueEmployee");
            
            CreateTable(
                "dbo.Employees",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(maxLength: 50),
                        LastName = c.String(maxLength: 100),
                        MiddleName = c.String(maxLength: 100),
                        Phone = c.String(maxLength: 30),
                        CompanyId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Companies", t => t.CompanyId)
                .Index(t => t.CompanyId);
            
            CreateTable(
                "dbo.CarWashEmployees",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EmployeeId = c.Int(nullable: false),
                        CarWashId = c.Int(nullable: false),
                        EmployeeRole = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CarWashes", t => t.CarWashId)
                .ForeignKey("dbo.Employees", t => t.EmployeeId)
                .Index(t => new { t.CarWashId, t.EmployeeId }, unique: true);
            
            CreateTable(
                "dbo.CarWashes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CompanyId = c.Int(nullable: false),
                        Name = c.String(maxLength: 100),
                        Description = c.String(maxLength: 500),
                        StartTimeOfWork = c.Time(nullable: false, precision: 7),
                        EndTimeOfWork = c.Time(nullable: false, precision: 7),
                        Phone = c.String(maxLength: 30),
                        City = c.String(maxLength: 100),
                        Street = c.String(maxLength: 200),
                        HouseNumber = c.String(maxLength: 20),
                        Latitude = c.Double(nullable: false),
                        Longitude = c.Double(nullable: false),
                        State = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Companies", t => t.CompanyId)
                .Index(t => t.CompanyId);
            
            CreateTable(
                "dbo.CarWashAdditionalWorks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CarWashId = c.Int(nullable: false),
                        WorkId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CarWashes", t => t.CarWashId)
                .ForeignKey("dbo.Works", t => t.WorkId)
                .Index(t => new { t.CarWashId, t.WorkId }, unique: true);
            
            CreateTable(
                "dbo.Works",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CompanyId = c.Int(),
                        Name = c.String(nullable: false, maxLength: 150),
                        Description = c.String(maxLength: 500),
                        SortOrder = c.Byte(nullable: false),
                        WorkState = c.Int(nullable: false),
                        WorkType = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Companies", t => t.CompanyId)
                .Index(t => t.CompanyId);
            
            CreateTable(
                "dbo.Companies",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 50),
                        Description = c.String(maxLength: 500),
                        Balance = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Tariffs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        WorkId = c.Int(nullable: false),
                        CarWashId = c.Int(nullable: false),
                        Cost = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Duration = c.Int(nullable: false),
                        CarType = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CarWashes", t => t.CarWashId)
                .ForeignKey("dbo.Works", t => t.WorkId)
                .Index(t => new { t.CarType, t.CarWashId, t.WorkId }, unique: true, name: "IX_CarType_WorkId_CarWashId");
            
            CreateTable(
                "dbo.Orders",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        TariffId = c.Int(nullable: false),
                        CarWashBoxId = c.Int(nullable: false),
                        DateFrom = c.DateTime(nullable: false),
                        DateTo = c.DateTime(nullable: false),
                        Cost = c.Decimal(nullable: false, precision: 18, scale: 2),
                        OrderType = c.Int(nullable: false),
                        OrderState = c.Int(nullable: false),
                        CarNumber = c.String(maxLength: 9),
                        CarMake = c.String(maxLength: 30),
                        ClientPhone = c.String(maxLength: 30),
                        ClientName = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CarWashBoxes", t => t.CarWashBoxId)
                .ForeignKey("dbo.Tariffs", t => t.TariffId)
                .Index(t => t.TariffId)
                .Index(t => t.CarWashBoxId);
            
            CreateTable(
                "dbo.CarWashBoxes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CarWashId = c.Int(nullable: false),
                        Name = c.String(maxLength: 30),
                        State = c.Int(nullable: false),
                        SortOrder = c.Byte(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CarWashes", t => t.CarWashId)
                .Index(t => t.CarWashId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Accounts", "EmployeeId", "dbo.Employees");
            DropForeignKey("dbo.Employees", "CompanyId", "dbo.Companies");
            DropForeignKey("dbo.CarWashEmployees", "EmployeeId", "dbo.Employees");
            DropForeignKey("dbo.CarWashEmployees", "CarWashId", "dbo.CarWashes");
            DropForeignKey("dbo.CarWashes", "CompanyId", "dbo.Companies");
            DropForeignKey("dbo.CarWashAdditionalWorks", "WorkId", "dbo.Works");
            DropForeignKey("dbo.Tariffs", "WorkId", "dbo.Works");
            DropForeignKey("dbo.Orders", "TariffId", "dbo.Tariffs");
            DropForeignKey("dbo.Orders", "CarWashBoxId", "dbo.CarWashBoxes");
            DropForeignKey("dbo.CarWashBoxes", "CarWashId", "dbo.CarWashes");
            DropForeignKey("dbo.Tariffs", "CarWashId", "dbo.CarWashes");
            DropForeignKey("dbo.Works", "CompanyId", "dbo.Companies");
            DropForeignKey("dbo.CarWashAdditionalWorks", "CarWashId", "dbo.CarWashes");
            DropIndex("dbo.CarWashBoxes", new[] { "CarWashId" });
            DropIndex("dbo.Orders", new[] { "CarWashBoxId" });
            DropIndex("dbo.Orders", new[] { "TariffId" });
            DropIndex("dbo.Tariffs", "IX_CarType_WorkId_CarWashId");
            DropIndex("dbo.Works", new[] { "CompanyId" });
            DropIndex("dbo.CarWashAdditionalWorks", new[] { "CarWashId", "WorkId" });
            DropIndex("dbo.CarWashes", new[] { "CompanyId" });
            DropIndex("dbo.CarWashEmployees", new[] { "CarWashId", "EmployeeId" });
            DropIndex("dbo.Employees", new[] { "CompanyId" });
            DropIndex("dbo.Accounts", "UniqueEmployee");
            DropIndex("dbo.Accounts", "UniqueEmail");
            DropTable("dbo.CarWashBoxes");
            DropTable("dbo.Orders");
            DropTable("dbo.Tariffs");
            DropTable("dbo.Companies");
            DropTable("dbo.Works");
            DropTable("dbo.CarWashAdditionalWorks");
            DropTable("dbo.CarWashes");
            DropTable("dbo.CarWashEmployees");
            DropTable("dbo.Employees");
            DropTable("dbo.Accounts");
        }
    }
}
