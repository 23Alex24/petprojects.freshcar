namespace FreshCar.Infrastructure.Core.Services.EF
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateTable_CarWashPhotos : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CarWashPhotos",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CarWashId = c.Int(nullable: false),
                        PhotoUrl = c.String(maxLength: 250),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CarWashes", t => t.CarWashId)
                .Index(t => t.CarWashId)
                .Index(t => t.PhotoUrl, unique: true, name: "IX_CarWashPhotoUrl");
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CarWashPhotos", "CarWashId", "dbo.CarWashes");
            DropIndex("dbo.CarWashPhotos", "IX_CarWashPhotoUrl");
            DropIndex("dbo.CarWashPhotos", new[] { "CarWashId" });
            DropTable("dbo.CarWashPhotos");
        }
    }
}
