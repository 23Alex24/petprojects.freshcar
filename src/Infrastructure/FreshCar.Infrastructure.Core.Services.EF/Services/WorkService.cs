﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using FreshCar.Core.Entities;
using FreshCar.Core.Services;
using FreshCar.Infrastructure.Core.Services.EF.Extensions;

namespace FreshCar.Infrastructure.Core.Services.EF.Services
{
    /// <summary>
    /// Сервис для работы с услугами и тарифами
    /// </summary>
    internal class WorkService : BaseService, IWorkService
    {
        public WorkService(IUnitOfWork unitOfWork) : base(unitOfWork)
        { }

        /// <summary>
        /// Возвращает список основных системных услуг. Только для чтения
        /// </summary>
        public async Task<IEnumerable<Work>> GetMainSystemWorksAsync()
        {
            var repository = UnitOfWork.GetRepository<Work>();

            return await repository.Query().AsNoTracking()
                .WithoutCompany()
                .FilterByState(WorkState.Active)
                .FilterByType(WorkType.MainSystem)
                .OrderBy(x => x.SortOrder)
                .ToListAsync();
        }

        /// <summary>
        /// Возврвщает список дополнительных системных услуг (кофе, wi-if...). Только для чтения
        /// </summary>
        public async Task<IEnumerable<Work>> GetAdditionalSystemWorksAsync()
        {
            var repository = UnitOfWork.GetRepository<Work>();

            return await repository.Query().AsNoTracking()
                .FilterByState(WorkState.Active)
                .WithoutCompany()
                .FilterByType(WorkType.AdditionalSystem)
                .OrderBy(x => x.SortOrder)
                .ToListAsync();
        }





        /// <summary>
        /// Возвращает все основные услуги фирмы (только активные). Только для чтения
        /// </summary>
        public async Task<Work[]> GetMainWorksAsync(int companyId)
        {
            var repository = UnitOfWork.GetRepository<Work>();

            return await repository.Query().AsNoTracking()
                .FilterByState(WorkState.Active)
                .FilterByCompanyId(companyId)
                .OnlyMainWorks()
                .NotDeleted()
                .ToArrayAsync();
        }

        /// <summary>
        /// Возвращает список доп услуг, присутствующих у автомойки
        /// </summary>
        public async Task<IEnumerable<Work>> GetAdditionalWorksAsync(int carWashId, bool readOnly)
        {
            var repository = UnitOfWork.GetRepository<CarWashAdditionalWork>();

            var query = repository.Query();

            if (readOnly)
                query = query.AsNoTracking();

            return await query.Where(x => x.CarWashId == carWashId)
                .Select(x => x.Work)
                .OnlyAdditionalWorks()
                .FilterByState(WorkState.Active)                
                .OrderBy(x => x.SortOrder)
                .ToListAsync();
        }

        /// <summary>
        /// Возвращает список доп услуг, присутствующих у автомойки
        /// </summary>
        public async Task<CarWashAdditionalWork[]> GetAdditionalWorksLinksAsync(int carWashId, bool readOnly)
        {
            var repository = UnitOfWork.GetRepository<CarWashAdditionalWork>();
            var query = repository.Query();

            if (readOnly)
                query = query.AsNoTracking();

            return await query.Where(x => x.CarWashId == carWashId).ToArrayAsync();
        }


        /// <summary>
        /// Возвращает услугу по айди
        /// </summary>
        public async Task<Work> GetMainWorkAsync(int workId, bool readOnly)
        {
            var repository = UnitOfWork.GetRepository<Work>();
            var query = repository.Query();

            if (readOnly)
                query = query.AsNoTracking();

            return await query
                .FilterByState(WorkState.Active)
                .FilterById(workId)
                .FirstOrDefaultAsync();
        }

        /// <summary>
        /// Get Work by Id
        /// </summary>
        public async Task<Work> GetMainWorkAsync(int workId, int companyId)
        {
            var repository = UnitOfWork.GetRepository<Work>();

            var work = await repository.Query()
                .FilterByState(WorkState.Active)
                .FilterById(workId)
                .OnlyMainWorks()
                .FilterByCompanyId(companyId)
                .FirstOrDefaultAsync();

            return work;
        }

        /// <summary>
        /// Проверяет, есть ли уже у автомойки основная услуга с таким названием
        /// </summary>
        public async Task<bool> MainWorkIsExistAsync(int companyId, string name)
        {
            var repository = UnitOfWork.GetRepository<Work>();

            return await repository.Query().AsNoTracking()
                .FilterByState(WorkState.Active)
                .FilterByCompanyId(companyId)
                .FilterByName(name)
                .AnyAsync();
        }

        /// <summary>
        /// Проверяет, можно ли у услуги изменить имя на новое
        /// </summary>
        public async Task<bool> MainWorkCanEditNameAsync(int companyId, int workId, string name)
        {
            var repository = UnitOfWork.GetRepository<Work>();

            var any = await repository.Query().AsNoTracking()
                .Where(x => x.Id != workId)
                .FilterByState(WorkState.Active)
                .FilterByType(WorkType.MainCreatedByUser)
                .FilterByCompanyId(companyId)
                .FilterByName(name)
                .AnyAsync();

            return !any;
        }


        /// <summary>
        /// Ищет тариф по айдишнику
        /// </summary>
        public async Task<Tariff> GetTariffByIdAsync(int tariffId)
        {
            var repository = UnitOfWork.GetRepository<Tariff>();
            return await repository.Query()
                .Where(x => x.Id == tariffId)
                .FirstOrDefaultAsync();
        }

        /// <summary>
        /// Возвращает тариф
        /// </summary>
        /// <param name="carWashId">Id мойки, для которой надо найти тариф</param>
        /// <param name="workId">Id услуги</param>
        /// <param name="carType">Тип машины</param>
        public async Task<Tariff> GetTariffAsync(int carWashId, int workId, CarType carType)
        {
            var carWashRepository = UnitOfWork.GetRepository<CarWash>();

            var companyId = await carWashRepository.Query().AsNoTracking()
                .Where(x => x.Id == carWashId)
                .Select(x => x.CompanyId)
                .FirstOrDefaultAsync();

            if (companyId < 1)
                return null;

            var workRepository = UnitOfWork.GetRepository<Work>();
            var hasWork = await workRepository.Query().AsNoTracking()
                .FilterByState(WorkState.Active)
                .FilterById(workId)
                .FilterByCompanyId(companyId)
                .AnyAsync();

            if (!hasWork)
                return null;

            var tariffRepository = UnitOfWork.GetRepository<Tariff>();
            return await tariffRepository.Query().AsNoTracking()
                .FilterByCarType(carType)
                .Where(x => x.WorkId == workId)
                .Where(x=>x.CarWashId == carWashId)
                .FirstOrDefaultAsync();
        }

        /// <summary>
        /// Возвращает тарифы на услугу автомойки. Только для чтения
        /// </summary>
        /// <param name="carWashId">Id автомойки</param>
        /// <param name="workId">Id услуги</param>
        /// <param name="readOnly">true, если данные нужны только для чтения</param>
        public async Task<Tariff[]> GetTariffsAsync(int carWashId, int workId, bool readOnly)
        {
            var repository = UnitOfWork.GetRepository<Tariff>();

            var query = repository.Query();

            if (readOnly)
                query = query.AsNoTracking();

            return await query
                .Where(x => x.WorkId == workId && x.CarWashId == carWashId)
                .ToArrayAsync();
        }

        /// <summary>
        /// Возвращает тарифы. Только для чтения
        /// </summary>
        public async Task<TariffInfo[]> GetTariffsAsync(int[] tariffsId)
        {
            var repository = UnitOfWork.GetRepository<Tariff>();

            if (tariffsId != null)
            {
                return await repository.Query()
                    .AsNoTracking()
                    .Where(x => tariffsId.Contains(x.Id))
                    .Select(x => new TariffInfo
                    {
                        TariffId = x.Id,
                        CarType = x.CarType,
                        WorkId = x.WorkId,
                        WorkName = x.Work.Name,
                    })
                    .ToArrayAsync();
            }

            return null;
        }
    }
}
