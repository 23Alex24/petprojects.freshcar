﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using FreshCar.Core.Entities;
using FreshCar.Core.Services;
using FreshCar.Infrastructure.Core.Services.EF.Extensions;
using FreshCar.Utils.Extensions;

namespace FreshCar.Infrastructure.Core.Services.EF.Services
{
    /// <summary>
    /// Сервис для работы с заказами
    /// </summary>
    internal class OrderService :BaseService, IOrderService
    {
        public OrderService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        /// <summary>
        /// Возвращает заказы за указанную дату (все кроме отмененных и удаленных). Только для чтения
        /// </summary>
        public async Task<Order[]> GetOrders(int carWashId, DateTime date)
        {
            var startDate = date.ToUtc();
            var endDate = date.AddDays(1).AddSeconds(-1);
            var repository = UnitOfWork.GetRepository<Order>();

            return await repository.Query().AsNoTracking()
                .Where(x => x.CarWashBox.CarWashId == carWashId)
                .Where(x => x.CarWashBoxId != null)
                .Where(x => x.DateFrom >= startDate && x.DateFrom <= endDate)
                .NotDeleted()
                .NotCanceled()
                .ToArrayAsync();
        }

        /// <summary>
        /// Возвращает активный заказ (не завершенный, не отменный, не удаленный).
        /// </summary>
        /// <param name="readOnly">true, если данные нужны только для чтения, иначе false</param>
        public async Task<Order> GetActiveOrder(int carWashId, long orderId, bool readOnly)
        {
            var repository = UnitOfWork.GetRepository<Order>();
            var query = repository.Query();

            if (readOnly)
                query = query.AsNoTracking();

            return await query
                .OnlyActive()
                .Where(x => x.Id == orderId && x.CarWashBox.CarWashId == carWashId)
                .FirstOrDefaultAsync();
        }

        /// <summary>
        /// Проверяет можно ли изменить время заказа
        /// </summary>
        /// <param name="boxId">Id бокса</param>
        /// <param name="dateFrom">Дата начала заказа</param>
        /// <param name="dateTo">Дата окончания заказа</param>
        /// <param name="orderId">Id заказа, у которого хотят изменить даты</param>
        public async Task<bool> CanChangeOrderDates(int boxId, long orderId, DateTime dateFrom, DateTime dateTo)
        {
            var repository = UnitOfWork.GetRepository<Order>();
            var hasAnyOrder = await repository.Query().AsNoTracking()
                .OnlyActive()
                .Where(x => x.Id != orderId)
                .Where(x => x.CarWashBoxId == boxId)
                .FilterByDates(dateFrom, dateTo)
                .AnyAsync();

            return !hasAnyOrder;
        }

        /// <summary>
        /// Возвращает заявки для статистики. Только для чтения
        /// </summary>
        public async Task<Order[]> GetOrdersForStatistic(int carWashId, DateTime dateFrom, DateTime dateTo)
        {
            var repository = UnitOfWork.GetRepository<Order>();
            return await repository.Query().AsNoTracking()
                .Where(x => x.CarWashBox.CarWashId == carWashId)
                .Where(x => x.DateFrom >= dateFrom && x.DateTo <= dateTo)
                .ToArrayAsync();
        }
    }
}
