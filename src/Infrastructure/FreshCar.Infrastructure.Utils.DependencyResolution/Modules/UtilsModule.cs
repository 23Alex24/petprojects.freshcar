﻿using Autofac;
using FreshCar.Infrastructure.Utils.Cryptography;
using FreshCar.Infrastructure.Utils.Geolocation;
using FreshCar.Infrastructure.Utils.Logger;
using FreshCar.Infrastructure.Utils.Mapping;
using FreshCar.Utils.Cryptography;
using FreshCar.Utils.Geolocation;
using FreshCar.Utils.Logger;
using FreshCar.Utils.Mapping;

namespace FreshCar.Infrastructure.Utils.DependencyResolution.Modules
{
    /// <summary>
    /// Модуль, в котором находятся настройки DI для утилит
    /// </summary>
    public class UtilsModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<EmptyLoggerUtility>().As<ILoggerUtility>();
            builder.RegisterType<CryptographyUtility>().As<ICryptographyUtility>();
            builder.RegisterType<GeolocationUtility>().As<IGeolocationUtility>();
            builder.RegisterType<AutoMapperUtilityImpl>().As<IMapperUtility>();
        }
    }
}
