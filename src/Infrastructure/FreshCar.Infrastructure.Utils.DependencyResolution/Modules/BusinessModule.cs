﻿using Autofac;
using FreshCar.Business.Commands;
using FreshCar.Business.Commands.Box;
using FreshCar.Business.Commands.WorkCommands;
using FreshCar.Business.Commands.Order;

namespace FreshCar.Infrastructure.Utils.DependencyResolution.Modules
{
    /// <summary>
    /// Модуль, в котором находятся настройки DI для бизнес слоя
    /// </summary>
    public class BusinessModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<AuthorizeUserCommand>()
                .As<ICommand<AuthorizeUserCommandArgs, AuthorizeUserCommandResult>>();

            builder.RegisterType<RegisterUserCommand>()
                .As<ICommand<RegisterUserCommandArgs, EmptyCommandResult>>();

            builder.RegisterType<CreateWorkCommand>()
                .As<ICommand<CreateWorkArguments, EmptyCommandResult>>();

            builder.RegisterType<EditWorkTariffsCommand>()
                .As<ICommand<EditWorkTariffsArguments, EmptyCommandResult>>();

            builder.RegisterType<DeleteWorkCommand>()
                .As<ICommand<DeleteWorkArguments, EmptyCommandResult>>();

            builder.RegisterType<CreateOrderFromClientCommand>()
                .As<ICommand<CreateOrderFromClientArgs, EmptyCommandResult>>();

            builder.RegisterType<CreateOrderFromAdminCommand>()
                .As<ICommand<CreateOrderFromAdminCommandArgs, CreateOrderFromAdminCommandResult>>();

            builder.RegisterType<DeleteOrderCommand>()
                .As<ICommand<DeleteOrderCommandArgs, EmptyCommandResult>>();

            builder.RegisterType<EditOrderFromAdminCommand>()
                .As<ICommand<EditOrderFromAdminArgs, EmptyCommandResult>>();

            builder.RegisterType<CreateBoxCommand>()
                .As<ICommand<CreateBoxCommandArgs, EmptyCommandResult>>();

            builder.RegisterType<EditBoxCommand>()
                .As<ICommand<EditBoxCommandArgs, EmptyCommandResult>>();

            builder.RegisterType<DeleteBoxCommand>()
                .As<ICommand<DeleteBoxCommandArgs, EmptyCommandResult>>();

            builder.RegisterType<EditWorkCommand>()
                .As<ICommand<EditWorkCommandArgs, EmptyCommandResult>>();

            builder.RegisterType<EditCarWashCommand>()
                .As<ICommand<EditCarWashCommandArgs, EmptyCommandResult>>();

            builder.RegisterType<EditAdditionalWorksCommand>()
                .As<ICommand<EditAdditionalWorksArgs, EmptyCommandResult>>();

            builder.RegisterType<CreateNewEmployeeCommand>()
                .As<ICommand<CreateNewEmployeeCommandArgs, EmptyCommandResult>>();

            builder.RegisterType<EditEmployeeCommand>()
                .As<ICommand<EditEmployeeCommandArgs, EmptyCommandResult>>();

            builder.RegisterType<DeleteEmployeeCommand>()
                .As<ICommand<DeleteEmployeeCommandArgs, EmptyCommandResult>>();
        }
    }
}
