﻿namespace FreshCar.Business.Commands.WorkCommands
{
    /// <summary>
    /// Аргументы команды изменения услуги
    /// </summary>
    public class EditWorkTariffsArguments : BaseCommandArguments
    {
        public int WorkId { get; set; }

        public EditTariffArguments[] Tariffs { get; set; }
    }
}