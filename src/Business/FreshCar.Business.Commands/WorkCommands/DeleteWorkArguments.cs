﻿namespace FreshCar.Business.Commands.WorkCommands
{
    public class DeleteWorkArguments : BaseCommandArguments
    {
        public int Id { get; set; }
    }
}
