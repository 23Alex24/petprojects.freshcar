﻿namespace FreshCar.Business.Commands.WorkCommands
{
    public class EditTariffArguments : BaseCommandArguments
    {
        public int TariffId { get; set; }

        public decimal Cost { get; set; }

        public int Duration { get; set; }
    }
}
