﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FreshCar.Business.Commands.Extensions;
using FreshCar.Business.Errors;
using FreshCar.Core.Entities;
using FreshCar.Core.Services;
using FreshCar.Utils.Logger;

namespace FreshCar.Business.Commands.WorkCommands
{
    internal class EditAdditionalWorksCommand : BaseCommand<EditAdditionalWorksArgs, EmptyCommandResult>
    {
        private readonly IWorkService _workService;
        private readonly IUnitOfWork _unitOfWork;
        private IEnumerable<Work> _systemWorks;

        public EditAdditionalWorksCommand(ILoggerUtility logger, 
            IWorkService workService, 
            IUnitOfWork unitOfWork)
            : base(logger)
        {
            _workService = workService;
            _unitOfWork = unitOfWork;
        }

        protected override async Task<EmptyCommandResult> PerformCommand(EditAdditionalWorksArgs arguments)
        {           
            var result = new EmptyCommandResult { IsSuccess = true };
            var repository = _unitOfWork.GetRepository<CarWashAdditionalWork>();
            var carWashWorks = await _workService
                .GetAdditionalWorksLinksAsync(arguments.CurrentUserInfo.CarWashId.Value, false);

            for (int i = 0; i < arguments.CarWashAdditionalWorks.Length; i++)
            {
                var editedWork = arguments.CarWashAdditionalWorks[i];
                var dbWork = carWashWorks.FirstOrDefault(x => x.WorkId == editedWork.WorkId);

                //если есть запись в БД
                if (dbWork != null)
                {
                    if(editedWork.IsActive)
                        continue;

                    repository.Remove(dbWork);
                    continue;
                }

                //если нет в бд
                if(!editedWork.IsActive)
                    continue;

                repository.Add(new CarWashAdditionalWork()
                {
                    CarWashId = arguments.CurrentUserInfo.CarWashId.Value,
                    WorkId = editedWork.WorkId,                    
                });
            }

            await _unitOfWork.SaveChangesAsync();
            return result;
        }

        protected override async Task<EmptyCommandResult> Validate(EditAdditionalWorksArgs arguments)
        {
            var result = new EmptyCommandResult { IsSuccess = true };
            DistinctWorks(arguments);

            _systemWorks = await _workService.GetAdditionalSystemWorksAsync();

            foreach (var editedWork in arguments.CarWashAdditionalWorks)
            {
                var isExists = _systemWorks.Any(x => x.Id == editedWork.WorkId);
                if (!isExists)
                    return result.AddError(ErrorConstants.Work.WORK_NOT_FOUND, 404);
            }

            return result;
        }

        private void DistinctWorks(EditAdditionalWorksArgs arguments)
        {
            var ids = arguments.CarWashAdditionalWorks.Select(x => x.WorkId).Distinct().ToArray();
            var distinctedWorks = new List<EditAdditionalWorkArgs>();
            for (int i = 0; i < ids.Length; i++)
            {
                var work = arguments.CarWashAdditionalWorks.First(x => x.WorkId == ids[i]);
                distinctedWorks.Add(work);
            }
            arguments.CarWashAdditionalWorks = distinctedWorks.ToArray();
        }
    }
}
