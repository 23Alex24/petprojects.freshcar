﻿using System;
using System.Data;
using System.Threading.Tasks;
using FreshCar.Business.Commands.Extensions;
using FreshCar.Business.Commands.Extensions.Rules;
using FreshCar.Business.Errors;
using FreshCar.Core.Entities;
using FreshCar.Core.Services;
using FreshCar.Utils.Extensions;
using FreshCar.Utils.Logger;

namespace FreshCar.Business.Commands.Order
{
    /// <summary>
    /// Команда изменения заказа от имени администратора
    /// </summary>
    internal class EditOrderFromAdminCommand : BaseCommand<EditOrderFromAdminArgs, EmptyCommandResult>
    {
        #region Поля, конструктор 

        private readonly IOrderService _orderService;
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICompanyService _companyService;
        private readonly ICarWashBoxService _carWashBoxService;
        private readonly IWorkService _workService;
        private readonly ICarWashService _carWashService;
        private Core.Entities.Order _order;

        public EditOrderFromAdminCommand(ILoggerUtility loggerUtility, 
            IOrderService orderService,
            IUnitOfWork unitOfWork,
            ICompanyService companyService,
            ICarWashBoxService carWashBoxService,
            IWorkService workService,
            ICarWashService carWashService) : base(loggerUtility)
        {
            _orderService = orderService;
            _unitOfWork = unitOfWork;
            _companyService = companyService;
            _carWashBoxService = carWashBoxService;
            _workService = workService;
            _carWashService = carWashService;
        }

        #endregion



        protected override async Task<EmptyCommandResult> PerformCommand(EditOrderFromAdminArgs arguments)
        {
            var result = new EmptyCommandResult() {IsSuccess = true};

            //Если отменили, то просто меняем статус
            if (arguments.OrderState == OrderState.CanceledByAdministrator)
            {
                _order.OrderState = OrderState.CanceledByAdministrator;
                await _unitOfWork.SaveChangesAsync();
                return result;
            }

            //если текущий статус не оплачено, и надо его закрыть, то просто списываем баланс если пришло с приложухи
            if (_order.OrderState == OrderState.FinishedAndNotPaid)
            {
                if (arguments.OrderState == OrderState.FinishedAndPaid &&
                    _order.OrderType == OrderType.Automatic)
                {
                    await ChangeBalance(arguments);
                }

                _order.OrderState = OrderState.FinishedAndPaid;
                await _unitOfWork.SaveChangesAsync();
                return result;
            }

            try
            {
                _unitOfWork.BeginTransaction(IsolationLevel.RepeatableRead);

                if (arguments.OrderState == OrderState.FinishedAndPaid && _order.OrderType == OrderType.Automatic)
                {
                    await ChangeBalance(arguments);
                }

                _order.OrderState = arguments.OrderState;
                _order.ClientPhone = arguments.ClientPhone;
                _order.CarMake = arguments.CarMake;
                _order.CarNumber = arguments.CarNumber;
                _order.DateTo = arguments.DateTo;

                if (_order.OrderState != OrderState.InWork)
                {
                    _order.DateFrom = arguments.DateFrom;
                    _order.CarWashBoxId = arguments.CarWashBoxId;
                    _order.Cost = arguments.Cost;
                    _order.TariffId = arguments.TariffId;
                }

                await _unitOfWork.SaveChangesAsync();
                _unitOfWork.CommitTransaction();
            }
            catch (Exception)
            {
                _unitOfWork.RollBackTransaction();
                throw;
            }
                                    
            return result;
        }



        protected override async Task<EmptyCommandResult> Validate(EditOrderFromAdminArgs arguments)
        {
            var result = new EmptyCommandResult() {IsSuccess = true};
            arguments.DateFrom = arguments.DateFrom.ToUtc();
            arguments.DateTo = arguments.DateTo.ToUtc();
            
            if (!StateIsAllowed(arguments))
            {
                return result.AddError(ErrorConstants.Order.INCORRECT_ORDER_STATE, 1000);
            }

            //сервис возвращает только активные заказы
            _order = await _orderService.GetActiveOrder(arguments.CurrentUserInfo.CarWashId.Value, arguments.Id, false);

            if (_order == null)
            {
                return result.AddError(ErrorConstants.Order.ORDER_NOT_FOUND, 404);
            }

            //Если хотят отменить заказ, то просто позволяем это сделать
            if (arguments.OrderState == OrderState.CanceledByAdministrator)
            {
                if (_order.OrderState == OrderState.FinishedAndNotPaid)
                {
                    return result.AddError("Нельзя отменить выполненный заказ", 1000);
                }

                return result;
            }

            //проверяем валидность нового статуса
            if (!NewStateIsValid(_order, arguments))
            {
                return result.AddError(ErrorConstants.Order.INCORRECT_ORDER_STATE, 1000);
            }           

            if (_order.OrderState == OrderState.InWork)
            {
                return result;
            }

            if (_order.OrderState == OrderState.FinishedAndNotPaid)
            {
                return result;
            }

            //Изменение основных данных
            //
            if (!DatesIsValid(arguments))
            {
                return result.AddError(ErrorConstants.Order.INCORRECT_DATES, 1000);
            }

            var carWash = await _carWashService.GetCarWashAsync(arguments.CurrentUserInfo.CarWashId.Value);
            if (!carWash.IsWorking(arguments.DateFrom.TimeOfDay, arguments.DateTo.TimeOfDay))
            {
                return result.AddError(ErrorConstants.CarWash.CAR_WASH_NOT_WORKING_ON_THIS_TIME, 400);
            }

            if (!await BoxIsValid(arguments))
            {
                return result.AddError(ErrorConstants.CarWash.BOX_NOT_FOUND, 404);
            }

            if (!await TariffIsValid(arguments))
            {
                return result.AddError(ErrorConstants.Tariff.TARIFF_NOT_FOUND, 404);
            }

            if (!await BoxIsFree(arguments))
            {
                return result.AddError(ErrorConstants.CarWash.BOX_NOT_FREE, 1000);
            }            

            return result;
        }





        #region Вспомогательные методы и классы

        /// <summary>
        /// Проверяет, что бокс валидный
        /// </summary>
        private async Task<bool> BoxIsValid(EditOrderFromAdminArgs arguments)
        {
            var box = await _carWashBoxService.GetBox(arguments.CarWashBoxId, true);

            if (box == null || box.CarWashId != arguments.CurrentUserInfo.CarWashId.Value ||
                box.State != CarWashBoxState.Works)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Проверяет, что тариф валидный
        /// </summary>
        private async Task<bool> TariffIsValid(EditOrderFromAdminArgs arguments)
        {
            var tariff = await _workService.GetTariffByIdAsync(arguments.TariffId);

            if (tariff == null || tariff.CarWashId != arguments.CurrentUserInfo.CarWashId.Value)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Проверяет, что бокс свободен
        /// </summary>
        private async Task<bool> BoxIsFree(EditOrderFromAdminArgs arguments)
        {
            var boxIsFree = await _orderService
                .CanChangeOrderDates(arguments.CarWashBoxId, arguments.Id, arguments.DateFrom, arguments.DateTo);

            if (!boxIsFree)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Проверяет, что даты валидные
        /// </summary>
        private bool DatesIsValid(EditOrderFromAdminArgs arguments)
        {
            var today = DateTime.Today.AddDays(-2);

            //запрещаем редактировать записи за предыдущие дни
            if (arguments.DateFrom < today || arguments.DateTo < arguments.DateFrom)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Проверяет является ли статус допустимым
        /// </summary>
        private bool StateIsAllowed(EditOrderFromAdminArgs arguments)
        {
            //эти статусы нельзя менять, для удаления есть отдельная команда
            if (arguments.OrderState == OrderState.CanceledByClient ||
                arguments.OrderState == OrderState.Deleted)
            {
                return false;
            }

            return true;
        }

        private bool NewStateIsValid(Core.Entities.Order order, EditOrderFromAdminArgs arguments)
        {
            if (_order.OrderState == OrderState.InWork)
            {
                //Если заказ в работе, то поменять можно только на эти статусы
                if (arguments.OrderState != OrderState.InWork &&
                    arguments.OrderState != OrderState.FinishedAndPaid &&
                    arguments.OrderState != OrderState.FinishedAndNotPaid)
                {
                    return false;
                }
            }

            //проверяем что новый статус валиден
            if (_order.OrderState == OrderState.FinishedAndNotPaid)
            {
                if (arguments.OrderState != OrderState.FinishedAndNotPaid &&
                    arguments.OrderState != OrderState.FinishedAndPaid)
                {
                    return false;
                }
            }

            return true;
        }

        private async Task ChangeBalance(EditOrderFromAdminArgs arguments)
        {
            var company = await _companyService.GetCompany(arguments.CurrentUserInfo.CompanyId.Value, false);
            company.Balance -= 25;
            _order.OrderState = OrderState.FinishedAndPaid;
        }

        #endregion
    }
}
