﻿using System;
using System.Threading.Tasks;
using FreshCar.Business.Commands.Extensions;
using FreshCar.Business.Commands.Extensions.Rules;
using FreshCar.Business.Errors;
using FreshCar.Core.Entities;
using FreshCar.Core.Services;
using FreshCar.Utils.Extensions;
using FreshCar.Utils.Logger;

namespace FreshCar.Business.Commands.Order
{
    /// <summary>
    /// Команда создания заявки на мойку от клиента (через приложение)
    /// </summary>
    internal class CreateOrderFromClientCommand : BaseCommand<CreateOrderFromClientArgs, EmptyCommandResult>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICarWashBoxService _carWashBoxService;
        private readonly IWorkService _workService;
        private readonly ICarWashService _carWashService;
        private CarWashBox _freeBox;
        private Tariff _tariff;

        public CreateOrderFromClientCommand(
            ILoggerUtility loggerUtility, 
            IUnitOfWork unitOfWork,
            ICarWashBoxService carWashBoxService,
            IWorkService workService,
            ICarWashService carWashService) : base(loggerUtility)
        {
            _unitOfWork = unitOfWork;
            _carWashBoxService = carWashBoxService;
            _workService = workService;
            _carWashService = carWashService;
        }

        protected override async Task<EmptyCommandResult> PerformCommand(CreateOrderFromClientArgs arguments)
        {
            var result = new EmptyCommandResult() { IsSuccess = true };
            var orderRepository = _unitOfWork.GetRepository<Core.Entities.Order>();

            var newOrder = new Core.Entities.Order
            {
                CarNumber = arguments.CarNumber,
                CarWashBoxId = _freeBox.Id,
                ClientPhone = arguments.ClientPhone,
                Cost = _tariff.Cost,
                OrderState = OrderState.New,
                DateFrom = arguments.StartDate,
                DateTo = arguments.StartDate.AddMinutes(_tariff.Duration),
                OrderType = OrderType.Automatic,
                TariffId = _tariff.Id,
            };

            orderRepository.Add(newOrder);
            await _unitOfWork.SaveChangesAsync();
            return result;
        }

        protected override async Task<EmptyCommandResult> Validate(CreateOrderFromClientArgs arguments)
        {
            var result = new EmptyCommandResult() {IsSuccess = true};

            arguments.StartDate = arguments.StartDate.ToUtc();

            //проверяем что тариф существует
            _tariff = await _workService.GetTariffByIdAsync(arguments.TariffId);

            if (_tariff == null)
            {
                return result.AddError(ErrorConstants.Tariff.TARIFF_NOT_FOUND, 404);
            }

            //проверяем, что автомойка существует и работает
            var carWash = await _carWashService.GetCarWashAsync(_tariff.CarWashId);
            if (carWash == null || carWash.State != CarWashState.Works)
            {
                return result.AddError(ErrorConstants.CarWash.CAR_WASH_NOT_FOUND, 404);
            }
          
            //проверяем что автомойка работает в это время
            var endDate = arguments.StartDate.AddMinutes(_tariff.Duration);

            if (!carWash.IsWorking(arguments.StartDate.TimeOfDay, endDate.TimeOfDay))
            {
                return result.AddError(ErrorConstants.CarWash.CAR_WASH_NOT_WORKING_ON_THIS_TIME, 1000);
            }

            //ищем свободный бокс
            _freeBox = await GetFreeBox(arguments.StartDate, endDate);

            if (_freeBox == null)
            {
                return result.AddError(ErrorConstants.CarWash.CAR_WASH_NOT_FREE, 1000);
            }

            return result;
        }
        
        private async Task<CarWashBox> GetFreeBox(DateTime dateFrom, DateTime dateTo)
        {
            CarWashBox result = null;
            var boxes = await _carWashService.GetCarWashBoxesAsync(_tariff.CarWashId);

            for (int i = 0; i < boxes.Length; i++)
            {
                var box = boxes[i];
                var boxIsFree = await _carWashBoxService.BoxIsFree(box.Id, dateFrom, dateTo);

                if (boxIsFree)
                {
                    result = box;
                    break;
                }
            }

            return result;
        }
    }
}
