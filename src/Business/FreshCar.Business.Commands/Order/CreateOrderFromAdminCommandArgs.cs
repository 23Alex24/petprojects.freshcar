﻿using System;

namespace FreshCar.Business.Commands.Order
{
    /// <summary>
    /// Аргументы для создания заказа от имени администратора
    /// </summary>
    public class CreateOrderFromAdminCommandArgs : BaseCommandArguments
    {
        /// <summary>
        /// Id тарифа
        /// </summary>
        public int TariffId { get; set; }

        /// <summary>
        /// Id бокса мойки, где будет происходить выполнение заказа
        /// </summary>
        public int CarWashBoxId { get; set; }

        /// <summary>
        /// Дата и время, когда начнется выполнение заказа (обязательно UTC)
        /// </summary>
        public DateTime DateFrom { get; set; }

        /// <summary>
        /// Номер машины клиента
        /// </summary>
        public string CarNumber { get; set; }

        /// <summary>
        /// Телефон клиента
        /// </summary>
        public string ClientPhone { get; set; }

        /// <summary>
        /// Марка автомобиля
        /// </summary>
        public string CarMake { get; set; }
    }
}
