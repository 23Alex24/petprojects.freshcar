﻿using System;
using System.Runtime.Remoting;
using System.Threading.Tasks;
using FreshCar.Business.Commands.Extensions;
using FreshCar.Business.Commands.Extensions.Rules;
using FreshCar.Business.Errors;
using FreshCar.Core.Entities;
using FreshCar.Core.Services;
using FreshCar.Utils.Extensions;
using FreshCar.Utils.Logger;

namespace FreshCar.Business.Commands.Order
{
    /// <summary>
    /// Команда создания заказа от имени администратора
    /// </summary>
    internal class CreateOrderFromAdminCommand : BaseCommand<CreateOrderFromAdminCommandArgs, CreateOrderFromAdminCommandResult>
    {
        private readonly ICarWashBoxService _carWashBoxService;
        private readonly IWorkService _workService;
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICarWashService _carWashService;
        private Tariff _tariff;

        public CreateOrderFromAdminCommand(ILoggerUtility loggerUtility,
            ICarWashBoxService carWashBoxService,
            IWorkService workService,
            IUnitOfWork unitOfWork,
            ICarWashService carWashService) : base(loggerUtility)
        {
            _carWashBoxService = carWashBoxService;
            _workService = workService;
            _unitOfWork = unitOfWork;
            _carWashService = carWashService;
        }

        protected override async Task<CreateOrderFromAdminCommandResult> PerformCommand(CreateOrderFromAdminCommandArgs arguments)
        {
            var newOrder = new Core.Entities.Order
            {
                CarMake = arguments.CarMake,
                CarNumber = arguments.CarNumber,
                CarWashBoxId = arguments.CarWashBoxId,
                ClientPhone = arguments.ClientPhone,
                Cost = _tariff.Cost,
                DateFrom = arguments.DateFrom,
                DateTo = arguments.DateFrom.AddMinutes(_tariff.Duration),
                OrderState = OrderState.New,
                OrderType = OrderType.Manual,
                TariffId = arguments.TariffId
            };

            var repository = _unitOfWork.GetRepository<Core.Entities.Order>();
            repository.Add(newOrder);
            await _unitOfWork.SaveChangesAsync();
            var result =  new CreateOrderFromAdminCommandResult() {IsSuccess = true};
            result.Id = newOrder.Id;
            return result;
        }

        
        protected override async Task<CreateOrderFromAdminCommandResult> Validate(CreateOrderFromAdminCommandArgs arguments)
        {
            var result = new CreateOrderFromAdminCommandResult() {IsSuccess = true};
            arguments.DateFrom = arguments.DateFrom.ToUtc();

            //нельзя создавать заказы за старые даты
            if (arguments.DateFrom < DateTime.UtcNow.AddDays(-2))
                return result.AddError(ErrorConstants.Order.INCORRECT_DATES, 400);

            //проверяем существует ли бокс и работает ли он
            var box = await _carWashBoxService.GetBox(arguments.CarWashBoxId, true);

            if (box == null || box.CarWashId != arguments.CurrentUserInfo.CarWashId.Value ||
                box.State != CarWashBoxState.Works)
            {
                return result.AddError(ErrorConstants.CarWash.BOX_NOT_FOUND, 404);
            }
                
            //проверяем существование тарифа
            _tariff = await _workService.GetTariffByIdAsync(arguments.TariffId);

            if (_tariff == null || _tariff.CarWashId != arguments.CurrentUserInfo.CarWashId.Value)
            {
                return result.AddError(ErrorConstants.Tariff.TARIFF_NOT_FOUND, 404);
            }

            //проверяем что автомойка работает в это время
            var carWash = await _carWashService.GetCarWashAsync(arguments.CurrentUserInfo.CarWashId.Value);
            var dateTo = arguments.DateFrom.AddMinutes(_tariff.Duration);

            if (!carWash.IsWorking(arguments.DateFrom.TimeOfDay, dateTo.TimeOfDay))
            {
                return result.AddError(ErrorConstants.CarWash.CAR_WASH_NOT_WORKING_ON_THIS_TIME, 1000);
            }

            //проверяем, что бокс свободен
            var boxIsFree = await _carWashBoxService
                .BoxIsFree(arguments.CarWashBoxId, arguments.DateFrom, dateTo);

            if (!boxIsFree)
            {
                return result.AddError(ErrorConstants.CarWash.BOX_NOT_FREE, 1000);
            }

            return result;
        }      
    }
}
