﻿namespace FreshCar.Business.Commands.Order
{
    public class CreateOrderFromAdminCommandResult : BaseCommandResult
    {
        /// <summary>
        /// Id созданного заказа
        /// </summary>
        public long Id { get; set; }
    }
}
