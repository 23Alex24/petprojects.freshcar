﻿using System;

namespace FreshCar.Business.Commands.Order
{
    public class CreateOrderFromClientArgs : BaseCommandArguments
    {
        /// <summary>
        /// Id тарифа
        /// </summary>
        public int TariffId { get; set; }

        /// <summary>
        /// Дата записи, когда начнется мойка
        /// </summary>
        public DateTime StartDate { get; set; }

        /// <summary>
        /// Номер машины
        /// </summary>
        public string CarNumber { get; set; }

        /// <summary>
        /// Телефон клиента
        /// </summary>
        public string ClientPhone { get; set; }
    }
}
