﻿namespace FreshCar.Business.Commands
{
    /// <summary>
    /// Аргументы для команды удаления сотрудника
    /// </summary>
    public class DeleteEmployeeCommandArgs : BaseCommandArguments
    {
        public int EmployeeId { get; set; }
    }
}
