﻿using System;
using System.Threading.Tasks;
using FreshCar.Business.Commands.Extensions;
using FreshCar.Business.Errors;
using FreshCar.Core.Entities;
using FreshCar.Core.Services;
using FreshCar.Utils.Logger;

namespace FreshCar.Business.Commands
{
    /// <summary>
    /// Команда удаления сотрудника
    /// </summary>
    public class DeleteEmployeeCommand : BaseCommand<DeleteEmployeeCommandArgs, EmptyCommandResult>
    {
        private readonly IAccountService _accountService;
        private readonly IEmployeeService _employeeService;
        private readonly IUnitOfWork _unitOfWork;
        private Employee _employee;
        private CarWashEmployee _carWashEmployee;

        public DeleteEmployeeCommand(
            IAccountService accountService,
            IEmployeeService employeeService, 
            IUnitOfWork unitOfWork,
            ILoggerUtility loggerUtility) : base(loggerUtility)
        {
            _accountService = accountService;
            _employeeService = employeeService;
            _unitOfWork = unitOfWork;
        }

        protected override async Task<EmptyCommandResult> PerformCommand(DeleteEmployeeCommandArgs arguments)
        {
            var result = new EmptyCommandResult() { IsSuccess = true };

            try
            {
                var account = await _accountService.GetEmployeeAccount(arguments.EmployeeId);

                _unitOfWork.BeginTransaction();

                var linkRepo = _unitOfWork.GetRepository<CarWashEmployee>();
                var employeeRepo = _unitOfWork.GetRepository<Employee>();

                linkRepo.Remove(_carWashEmployee);

                if (account != null)
                {
                    var accountRepo = _unitOfWork.GetRepository<Account>();
                    accountRepo.Remove(account);
                }

                employeeRepo.Remove(_employee);
                await _unitOfWork.SaveChangesAsync();
                _unitOfWork.CommitTransaction();
            }
            catch (Exception)
            {
                _unitOfWork.RollBackTransaction();
                throw;
            }

            return result;
        }

        protected override async Task<EmptyCommandResult> Validate(DeleteEmployeeCommandArgs arguments)
        {
            var result = new EmptyCommandResult() {IsSuccess = true};

            _employee = await _employeeService.GetEmployee(arguments.EmployeeId);

            if (_employee == null || _employee.CompanyId != arguments.CurrentUserInfo.CompanyId.Value)
                return result.AddError(ErrorConstants.Employee.EMPLOYEE_NOT_FOUND, 1000);

            _carWashEmployee = await _employeeService.GetCarWashEmployee(
                arguments.EmployeeId, arguments.CurrentUserInfo.CarWashId.Value);

            if (_carWashEmployee != null && _carWashEmployee.EmployeeRole == EmployeeRole.Director)
                return result.AddError(ErrorConstants.Employee.DIRECTOR_CANT_DELETED, 1001);

            return result;
        }
    }
}
