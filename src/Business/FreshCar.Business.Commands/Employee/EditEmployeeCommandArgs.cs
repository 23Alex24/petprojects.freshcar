﻿namespace FreshCar.Business.Commands
{
    /// <summary>
    /// Аргументы для команды изменения сотрудника
    /// </summary>
    public class EditEmployeeCommandArgs : BaseCommandArguments
    {
        public int EmployeeId { get; set; }

        /// <summary>
        /// Имя сотрудника
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Фамилия сотрудника
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Отчество сотрудника
        /// </summary>
        public string MiddleName { get; set; }

        /// <summary>
        /// Телефон сотрудника
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// Логин пользователя
        /// </summary>
        public string Login { get; set; }

        /// <summary>
        /// Пароль пользователя
        /// </summary>
        public string Password { get; set; }
    }
}
