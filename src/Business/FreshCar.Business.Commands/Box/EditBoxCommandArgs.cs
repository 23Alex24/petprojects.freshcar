﻿namespace FreshCar.Business.Commands.Box
{
    public class EditBoxCommandArgs : BaseCommandArguments
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
