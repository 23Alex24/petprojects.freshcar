﻿using System.Threading.Tasks;
using FreshCar.Business.Commands.Extensions;
using FreshCar.Business.Errors;
using FreshCar.Core.Entities;
using FreshCar.Core.Services;
using FreshCar.Utils.Logger;

namespace FreshCar.Business.Commands.Box
{
    internal class EditBoxCommand : BaseCommand<EditBoxCommandArgs, EmptyCommandResult>
    {
        private readonly ICarWashBoxService _carWashBoxService;
        private readonly IUnitOfWork _unitOfWork;
        private CarWashBox _box;

        public EditBoxCommand(ILoggerUtility loggerUtility,
            ICarWashBoxService carWashBoxService,
            IUnitOfWork unitOfWork) : base(loggerUtility)
        {
            _carWashBoxService = carWashBoxService;
            _unitOfWork = unitOfWork;
        }

        protected override async Task<EmptyCommandResult> PerformCommand(EditBoxCommandArgs arguments)
        {
            var result = new EmptyCommandResult() { IsSuccess = true };
            _box.Name = arguments.Name;
            await _unitOfWork.SaveChangesAsync();
            return result;
        }

        protected override async Task<EmptyCommandResult> Validate(EditBoxCommandArgs arguments)
        {
            var result = new EmptyCommandResult() {IsSuccess = true};

            _box = await _carWashBoxService.GetBox(arguments.Id, false);

            if (_box == null || _box.CarWashId != arguments.CurrentUserInfo.CarWashId.Value)
            {
                return result.AddError(ErrorConstants.CarWash.BOX_NOT_FOUND, 404);
            }

            return result;
        }
    }
}
