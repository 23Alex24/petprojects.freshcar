﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreshCar.Business.Commands.Box
{
    public class DeleteBoxCommandArgs : BaseCommandArguments
    {
        public int Id { get; set; }
    }
}
