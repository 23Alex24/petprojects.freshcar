﻿using FreshCar.Core.Services;

namespace FreshCar.Business.Commands
{
    /// <summary>
    /// Результат команды поиска аккаунта
    /// </summary>
    public class AuthorizeUserCommandResult : BaseCommandResult
    {
        /// <summary>
        /// Найденная информация по пользователю или null если пользователь не найден
        /// </summary>
        public UserInfo UserInfo { get; set; }
    }
}
