﻿using System.Threading.Tasks;
using FreshCar.Business.Commands.Extensions;
using FreshCar.Business.Errors;
using FreshCar.Core.Entities;
using FreshCar.Core.Services;
using FreshCar.Utils.Cryptography;
using FreshCar.Utils.Logger;

namespace FreshCar.Business.Commands
{
    /// <summary>
    /// Команда авторизации пользователя
    /// </summary>
    internal class AuthorizeUserCommand : BaseCommand<AuthorizeUserCommandArgs, AuthorizeUserCommandResult>
    {
        private readonly ICryptographyUtility _cryptographyUtility;
        private readonly IAccountService _accountService;
        private Account _account;

        public AuthorizeUserCommand(
            ILoggerUtility loggerUtility,
            ICryptographyUtility cryptographyUtility,
            IAccountService accountService) : base(loggerUtility)
        {
            _cryptographyUtility = cryptographyUtility;
            _accountService = accountService;
        }

        protected override async Task<AuthorizeUserCommandResult> PerformCommand(AuthorizeUserCommandArgs arguments)
        {
            var result = new AuthorizeUserCommandResult() { IsSuccess = true };
            result.UserInfo = await _accountService.GetUserInfoAsync(_account.Id);
            return result;
        }

        protected override async Task<AuthorizeUserCommandResult> Validate(AuthorizeUserCommandArgs arguments)
        {
            var result = new AuthorizeUserCommandResult() { IsSuccess = true };

            var hash = _cryptographyUtility.GetHash(arguments.Password);
            _account  = await _accountService.GetAccountAsync(arguments.Login, hash);

            if (_account == null)
            {
                result.AddError(ErrorConstants.Account.INVALID_LOGIN_OR_PASS, 10001);
                return result;
            }

            if (_account.AccountState == AccountState.NotVerified)
            {
                return result.AddError(ErrorConstants.Account.ACCOUNT_NOT_VERIFIED, 10005);
            }

            if (_account.AccountState != AccountState.Verified)
            {
                return result.AddError(ErrorConstants.Account.INVALID_ACCOUNT_STATUS, 10006);
            }

            return result;
        }
    }
}
